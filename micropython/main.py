# IMPORTS
import utime as time
from machine import I2C, Pin, RTC
from ht16k33matrix import HT16K33Matrix

# CONSTANTS
DELAY = 0.01
PAUSE = 3

REG_VERSION = 0
REG_SANITY = 1
REG_ID = 2
REG_BROADCAST_ID = 3     # which pendant to write to
REG_BROADCAST_ADDR = 4   # what address to write to
REG_BROADCAST_DATA = 5   # what to write
REG_TRIGGER_SCAN = 6     # set this to 1 then wait for it to clear before reading data
REG_SCAN_DATA_START = 7  # where the structure data will be read

SANITY_CODE = 99
PENDANT_I2C_DEVICE = 20

####################################################################

def parse(d,pos,depth,order,container):
    pid = d[pos]
    if pid==0: return 1

    pend = {
        'id': d[pos],
        'order': order,
        'desc': []
    }

    size = d[pos+1]
    p = 2
    order=0
    while p<size:        
        p+=parse(d,pos+p,depth+1,order,pend['desc'])
        order+=1
    container.append(pend)
    return p

def calc_width(struct):
    w = 0
    if len(struct['desc'])>0:
        for dec in struct['desc']:
            w+=calc_width(dec)
        return w
    else:
        return 2
    
def render_struct(struct,display,x,y):
    display.plot(7-x,y,"#")
    y+=2
    oldx=x
    for i,desc in enumerate(struct['desc']):
        render_struct(desc,display,x,y)
        x+=calc_width(desc)
        if len(struct['desc'])==1:
            display.plot(7-oldx,y-1,"#")
            
        if i<len(struct['desc'])-1:
            for xx in range(oldx,x+1):
                display.plot(7-xx,y-1,"#")
        
        oldx=x


####################################################################


def read_int(i2c,addr):
    return int.from_bytes(i2c.readfrom_mem(PENDANT_I2C_DEVICE, addr, 1),"little")

reads = []

def get_longest(l):
    m = 0
    ret = False
    for e in l:
        if len(e)>m:
            ret = e
            m = len(e)
    return ret

def read_pendant(i2c,display):
    global reads
        
    c = i2c.readfrom_mem(PENDANT_I2C_DEVICE, REG_SANITY, 1)
    if int.from_bytes(c,"little")==SANITY_CODE:
        # we have a pendant

        # trigger a scan and wait for it to complete
        i2c.writeto_mem(PENDANT_I2C_DEVICE, REG_TRIGGER_SCAN, b'\x00')
        scanning = i2c.readfrom_mem(PENDANT_I2C_DEVICE, REG_TRIGGER_SCAN, 1)
        while scanning==1:
            print("wait")
            scanning = i2c.readfrom_mem(PENDANT_I2C_DEVICE, REG_TRIGGER_SCAN, 1)
            sleep(0.2)
            
        pid = read_int(i2c,REG_SCAN_DATA_START)
        size = read_int(i2c,REG_SCAN_DATA_START+1)
        data = [pid,size]
        if size>2:
            for pos in range(0,size-2):
                data.append(read_int(i2c,REG_SCAN_DATA_START+pos+2))
        print(data)

        reads.append(data)
        if len(reads)>10: reads = reads[1:]

        if size>0:
            longest = get_longest(reads)
            struct=[]
            display.clear()
            parse(longest,0,0,0,struct)
            render_struct(struct[0],display,0,0)
            display.draw()
        

##################################################################
        
# START
if __name__ == '__main__':
    screen_i2c = I2C(0, scl=Pin(21), sda=Pin(20))    # Raspberry Pi Pico

    khipu_i2c=I2C(1,sda=Pin(2), scl=Pin(3), freq=400000)


    display = HT16K33Matrix(screen_i2c)
    display.set_brightness(10)

    # Draw a custom icon on the LED
    icon = b"\x3C\x42\xA9\x85\x85\xA9\x42\x3C"
    display.set_icon(icon).draw()



    while 1:
    
        print('Scanning I2C bus.')
        devices = khipu_i2c.scan() # this returns a list of devices

        device_count = len(devices)
    
        if device_count == 0:
            print('No i2c device found.')
        else:
            print(device_count, 'devices found.')
            if device_count==1:
                for device in devices:
                    if device==PENDANT_I2C_DEVICE:
                        khipu_i2c.writeto_mem(PENDANT_I2C_DEVICE, REG_BROADCAST_DATA, b'\x01')
                        khipu_i2c.writeto_mem(PENDANT_I2C_DEVICE, REG_BROADCAST_ADDR, b'\x00')
                        khipu_i2c.writeto_mem(PENDANT_I2C_DEVICE, REG_BROADCAST_ID, b'\x02')


                        read_pendant(khipu_i2c,display);


        time.sleep(0.6232)
