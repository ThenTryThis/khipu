// Copyright (C) 2021 FoAM Kernow
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include "usiTwiSlave.h"
#include "i2cmaster.h"
#include "i2cmaster2.h"
#include "i2cmaster3.h"
#include "i2cmaster4.h"
#include <stdint.h>
#include <avr/eeprom.h>

#define THIS_ID 0x01 // unique to all pendants
#define LED PB0
#define HAPTIC PA7
#define THIS_I2C_ADDR 0x14 // hardcoded to all pendants
#define SCAN_ADDR (THIS_I2C_ADDR<<1) // all pendants share same address
#define FIRMWARE_VERSION 2

//               attiny84
//                 ____
//          vcc  =|o   |=  gnd
//   ledr <- b0  =|    |=  a0 -> i2c2 scl
//   ledg <- b1  =|    |=  a1 -> i2c2 sda1
//         reset =|    |=  a2 -> i2c2 ada2
//   ledb <- b2  =|    |=  a3 -> i2c2 sda3
// haptic <- a7  =|    |=  a4 -> scl 
//    sda <- a6  =|____|=  a5 -> i2c2 sda4  <--- purple pin (white is reset)
//

#define REG_VERSION  0
#define REG_SANITY 1
#define REG_ID 2
#define REG_BROADCAST_ID 3     // which pendant to write to
#define REG_BROADCAST_ADDR 4   // what address to write to
#define REG_BROADCAST_DATA 5   // what to write
#define REG_TRIGGER_SCAN 6     // set this to 1 then wait for it to clear before reading data
#define REG_SCAN_DATA_START 7  // where the structure data will be read
#define MAXIMUM_COMPLEXITY 255
#define SANITY_CODE 99

// broadcast data is sent to all subsiduries and will only be acted on by the one that matches ID
unsigned char broadcast_id = 0;
unsigned char broadcast_addr = 0;
unsigned char broadcast_data = 0;

// state is data that can be written to via broadcasts
#define STATE_LED 0
#define STATE_HAPTIC_DURATION 1
#define STATE_HAPTIC_PATTERN 2
unsigned char state[3];

// the data that describes the structure of the 'downwards' pendants
unsigned char scan_data[MAXIMUM_COMPLEXITY];
unsigned char do_scan_now=0;

unsigned char read_i2c(unsigned char port, unsigned char dev, unsigned char addr, unsigned char *value);
unsigned char write_i2c(unsigned char port, unsigned char dev, unsigned char addr, unsigned char value);

uint8_t this_i2c_read(uint8_t reg) {
  // start by exposing scan data
  if (reg>=REG_SCAN_DATA_START) return scan_data[reg-REG_SCAN_DATA_START];
  
  switch (reg) {
  case REG_VERSION: return FIRMWARE_VERSION; 
  case REG_SANITY: return SANITY_CODE; 
  case REG_ID: return THIS_ID;
  case REG_TRIGGER_SCAN: return do_scan_now; // is scan happening?
  default: return 99;
  }
}

void this_i2c_write(uint8_t reg, uint8_t value) {
  switch (reg) {

  case REG_BROADCAST_ID: {
    broadcast_id = value;
    if (broadcast_id == THIS_ID) {
      state[broadcast_addr]=broadcast_data;
    }
  } break;
  case REG_BROADCAST_ADDR: {
    broadcast_addr = value;
  } break;
  case REG_BROADCAST_DATA: {
    broadcast_data = value;
  } break;
  case REG_TRIGGER_SCAN: {
    do_scan_now=1;
  } break;
  default: break;
  }
}

// empty pendant:
// -->
//
// [ id 6 0 0 0 0 ] (size is inclusive of id and size itself)

// some subsidaries:
// --> --> -->
//        \-->
//
// [1 21 [2 16 [3 4 0 0 0 0] [4 4 0 0 0 0] 0 0] 0 0 0]]


// read all connected 'downward' pendants and concatenate their structures together
// also sends broadcast messages while we are connected to them
void scan() {  
  scan_data[0]=THIS_ID;
  scan_data[1]=0;
  unsigned char value=0;
  unsigned int pos=2;

  PORTB |= _BV(LED);
  _delay_ms(20);
  PORTB &= ~_BV(LED);
  _delay_ms(20);

  // scan each port in turn
  for (unsigned int port=0; port<4; port++) {
    if (read_i2c(port, SCAN_ADDR, REG_SANITY, &value) && value==SANITY_CODE) {
      
      PORTB |= _BV(LED);
      _delay_ms(100);
      PORTB &= ~_BV(LED);
      _delay_ms(100);

      // pass on broadcast data
      write_i2c(port, SCAN_ADDR, REG_BROADCAST_ID, broadcast_id);
      write_i2c(port, SCAN_ADDR, REG_BROADCAST_ADDR, broadcast_addr);
      write_i2c(port, SCAN_ADDR, REG_BROADCAST_DATA, broadcast_data);

      // trigger and wait
      write_i2c(port, SCAN_ADDR, REG_TRIGGER_SCAN, 1);
      unsigned char scanning=1;
      while (scanning) {
        read_i2c(port, SCAN_ADDR, REG_TRIGGER_SCAN, &scanning); // id
        _delay_ms(1);
      }
      
      read_i2c(port, SCAN_ADDR, REG_SCAN_DATA_START, &scan_data[pos++]); // id
      unsigned char size=0;
      read_i2c(port, SCAN_ADDR, REG_SCAN_DATA_START+1, &size); // size
      scan_data[pos++]=size;
      // todo check size=0 and... wait???
      
      // read their scan data into our scan data
      for (unsigned int i=2; i<size; i++) {
        read_i2c(port, SCAN_ADDR, REG_SCAN_DATA_START+i, &scan_data[pos++]);
      }
      
    } else {
      scan_data[pos++]=0; // empty id
    }

    // lastly write the size
    scan_data[1]=pos;
  }
}

unsigned char haptic_position = 0;
unsigned char haptic_counter = 0;

void update_state() {
  if (state[STATE_LED]==1) {
    PORTB|=_BV(LED);
  } else {
    PORTB &= ~_BV(LED);
  }      

  if (haptic_counter==0) {
    if ((state[STATE_HAPTIC_PATTERN]>>haptic_position)&0x01) {
      PORTA |= _BV(HAPTIC);
    } else {
      PORTA &= ~_BV(HAPTIC);
    }
    haptic_position++;
    if (haptic_position>7) haptic_position=0;
    haptic_counter = state[STATE_HAPTIC_DURATION];
  }
  haptic_counter++;
  
}

int main() {
  usiTwiSlaveInit(THIS_I2C_ADDR, this_i2c_read, this_i2c_write);
  wdt_enable(WDTO_8S);
  sei();
  i2c_init();
  i2c2_init();
  i2c3_init();
  i2c4_init();

  //MCUSR = 0;
  //wdt_disable();
   
  DDRB |= _BV(LED); // led output  
  DDRA |= _BV(HAPTIC); // haptic output  
  
  for (unsigned int i=0; i<20; i++) {
    PORTB |= _BV(LED);
    _delay_ms(20);
    PORTB &= ~_BV(LED);
    _delay_ms(20);
  }

  // test motor
  for (unsigned int i=0; i<THIS_ID; i++) {
    PORTA |= _BV(HAPTIC);
    _delay_ms(50);
    PORTA &= ~_BV(HAPTIC); 
    _delay_ms(50);
  }

  // default states
  state[STATE_LED]=0;
  state[STATE_HAPTIC_DURATION]=10;
  state[STATE_HAPTIC_PATTERN]=0;
  
  while (1) {
    if (do_scan_now) {
      scan();
      do_scan_now=0;
    }
    update_state();
    _delay_ms(5);
    wdt_reset();
  }
  return 0;
}



// returns success
unsigned char read_i2c(unsigned char port, unsigned char dev, unsigned char addr, unsigned char *value) {
  switch (port) {
  case 0:
  if (!i2c_start(dev+I2C_WRITE)) {
    i2c_write(addr);               
    i2c_rep_start(dev+I2C_READ);  
    *value = i2c_readNak();             
    i2c_stop();
    return 1;
  }
  return 0;
  break;
  case 1:
  if (!i2c2_start(dev+I2C_WRITE)) {
    i2c2_write(addr);               
    i2c2_rep_start(dev+I2C_READ);  
    *value = i2c2_readNak();             
    i2c2_stop();
    return 1;
  }
  return 0;
  break;
  case 2:
  if (!i2c3_start(dev+I2C_WRITE)) {
    i2c3_write(addr);               
    i2c3_rep_start(dev+I2C_READ);  
    *value = i2c3_readNak();             
    i2c3_stop();
    return 1;
  }
  return 0;
  break;
  case 3:
  if (!i2c4_start(dev+I2C_WRITE)) {
    i2c4_write(addr);               
    i2c4_rep_start(dev+I2C_READ);  
    *value = i2c4_readNak();             
    i2c4_stop();
    return 1;
  }
  return 0;
  break;
  }

  return 0;
}

// returns success
unsigned char write_i2c(unsigned char port, unsigned char dev, unsigned char addr, unsigned char value) {
  switch (port) {
  case 0:
  if (!i2c_start(dev+I2C_WRITE)) {
    i2c_write(addr);               
    i2c_write(value);
    i2c_stop();
    return 1;
  }
  return 0;
  break;
  case 1:
  if (!i2c2_start(dev+I2C_WRITE)) {
    i2c2_write(addr);               
    i2c2_write(value);  
    i2c2_stop();
    return 1;
  }
  return 0;
  break;
  case 2:
  if (!i2c3_start(dev+I2C_WRITE)) {
    i2c3_write(addr);               
    i2c3_write(value);
    i2c3_stop();
    return 1;
  }
  return 0;
  break;
  case 3:
  if (!i2c4_start(dev+I2C_WRITE)) {
    i2c4_write(addr);
    i2c4_write(value);
    i2c4_stop();
    return 1;
  }
  return 0;
  break;
  }

  return 0;
}

