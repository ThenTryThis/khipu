// Copyright (C) 2021 FoAM Kernow
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include "usiTwiSlave.h"
#include "i2cmaster.h"
#include <stdint.h>
#include <avr/eeprom.h>

#define LED PB0
#define THIS_I2C_ADDR 0x14
#define FIRMWARE_VERSION 1

//            attiny84
//              ____
//       vcc  =|    |=  gnd
// led <- b0  =|    |=  a0 -> servo a
//        b1  =|    |=  a1 -> servo b
//      reset =|    |=  a2 -> servo c
//        b2  =|    |=  a3 -> servo d
//        a7  =|    |=  a4 -> scl
// sda <- a6  =|____|=  a5
//

// registers
#define REG_VERSION  0

uint8_t this_i2c_read(uint8_t reg) {
  PORTB |= _BV(LED);
  switch (reg) {
  case REG_VERSION: return FIRMWARE_VERSION; 
  default: return 99;
  }
}

void this_i2c_write(uint8_t reg, uint8_t value) {
  // write into crc registers directly
  switch (reg) {
  case 4: {
    if (value==1) {
      PORTB |= _BV(LED);
    } else {
      PORTB &= ~_BV(LED);
    }
  } break;
  default: break;
  }
}


int main() {
  usiTwiSlaveInit(THIS_I2C_ADDR, this_i2c_read, this_i2c_write);
  wdt_enable(WDTO_2S);
  sei();
  //  i2c_init();

  //MCUSR = 0;
  //wdt_disable();
 
  
  DDRB |= _BV(LED); // led output  

  for (unsigned int i=0; i<20; i++) {
    PORTB |= _BV(LED);
    _delay_ms(20);
    PORTB &= ~_BV(LED);
    _delay_ms(20);
  }

  wdt_reset();
  _delay_ms(1000);
  wdt_reset();

  //unsigned char test_addr = 0x14;
  //unsigned char addr = test_addr<<1;

  // write 0x75 to EEPROM address 5 (Byte Write) 
  
  while (1) {
    /*    unsigned char t = 0;

    // read previously written value back from EEPROM address 5 
    i2c_start_wait(addr+I2C_WRITE);     // set device address and write mode
    PORTB &= ~_BV(LED);
    i2c_write(0x99);                        // write address = 5
    i2c_rep_start(addr+I2C_READ);       // set device address and read mode
    t = i2c_readNak();                    // read one byte from EEPROM
    i2c_stop();

    PORTB |= _BV(LED);
    _delay_ms(40);
    PORTB &= ~_BV(LED);
    _delay_ms(40);
    PORTB |= _BV(LED);
    */
    _delay_ms(500);
	wdt_reset();
  }
  
  return 0;
}




