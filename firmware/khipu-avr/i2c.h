#include <avr/io.h>
#include <util/delay.h>

#ifndef I2CBB
#define I2CBB

// Port for the I2C
#define I2C_DDR DDRA
#define I2C_PIN PINA
#define I2C_PORT PORTA

// Pins to be used in the bit banging
#define I2C_CLK 0
#define I2C_DAT 1

void I2C_WriteBit(unsigned char c);
unsigned char I2C_ReadBit();

// Inits bitbanging port, must be called before using the functions below
void I2C_Init();

// Send a START Condition
void I2C_Start();

// Send a STOP Condition
void I2C_Stop();

// write a byte to the I2C slave device
unsigned char I2C_Write(unsigned char c);

// read a byte from the I2C slave device
unsigned char I2C_Read(unsigned char ack);

#endif
