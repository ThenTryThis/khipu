# Khipu interface design (and devlog)

A tangible interface that allows construction and manipulation of
hierarchical tree structures, inspired by khipu from the Inca
civilisation.

**Why?** We see the [limitations of our predominant thinking aids
(i.e. computational
technology)](http://worrydream.com/TheHumaneRepresentationOfThought/note.html)
as increasingly holding back our society from good decision making, by
relegating different forms of experience to "secondary status". In
order to counter this it is necessary to look to history for [other
societies who also use digital technologies to store, process and
manipulate information](https://khipumantes.github.io/).

As with our work on the [Penelope
project](https://penelope.hypotheses.org/), we consider the Inca as
holders of precious information and practises that have potential to
alleviate serious and widespread problems in the predominant
sociotechnical situation of today.

This is a prototype that elevates touch and physical manipulation to a
higher status in programming, up to now a field considered to exist in
a mythical plane of "abstract thought" known as "cyberspace". Perhaps
we can use it to explore the ways this strongly restricted,
disembodied thinking is in fact making us stupider, overall.

## Use cases

 * Livecoding musical performances
 * Working with visually impaired people as an accessible way to program
 * Incorporating Andean history into coding workshops with children
 * An experimental approach for navigating complex relationships (e.g. the [TTT Climate Tool](climate-tool.thentrythis.org))

### Microprocessor based approach

Pros:

 - A standard "straightforward" technique.
 - Made from relatively cheap parts.
 - Suitable for mass production.

Cons:

 - Requires lots of microprocessors, and obtaining them is getting harder.
 - Unfortunate terminology regarding serial communication, which we shall refer to as parent and child.

The attiny85 is one of the smallest microprocessors (and one we have
already used in the pattern matrix) with 8 pins. It comes with built
in hardware i2c serial communication, and a secondary serial bus can
be emulated on two of the remaining pins.

Using i2c we can theoretically support 112 addresses.

#### Diagram 1: 4 ring mini jack plug arrangement

![](https://codeberg.org/nebogeo/khipu/raw/branch/master/design/pics/cables.png)

Each pendant microprocessor would be responsible for reading all its
downward subsidiary "child" cords and reporting their ids (and
attached sub ids) to their parent. In the case of a cord attached to
the primary cord it would report it to the attached device via a USB
connection.

Each ID would be unique and would be mapped to the knots on the cord -
so the entire structure could be recreated in software. The downside
is that the knots would not directly be readable this way, but would
still provide a tactile method for organising information.

#### Diagram 2: ATTINY85 pinout

![](https://codeberg.org/nebogeo/khipu/raw/branch/master/design/pics/index.jpeg)

Pins 5 and 7 are used for the hardware i2c (sda and scl), which are
best suited to the upward parent connection, leaving e.g. pins 3 and 4
for a software i2c bus to connect "downward" to the child cords.

#### Scanning process

Each cord's microprocessor would await for a "scan" instruction (via a
register write), when detected it would issue scan instructions to all
detected cords on it's subsidiary bus. After waiting for all
subsidiary cords to indicate completion (via another register) it
would then read the ids reported for each subsidiary and indicate
completion to the parent.

#### Register map

| Address | Description | Notes | R/W |
|---------|-------------|-------|-----|
| 0x00    | Cord ID     | Same as i2c address | R |
| 0x01    | Start scan  |       | W |
| 0x02    | Scan complete |     | R |
| 0x03    | Start of cord data | | R |

#### Cord data

A nested data structure packed into bytes: 

| Offset | Name |
|--------|------|
| 0      | ID   |
| 1      | Size |
| 2      | Subsidiary 1 |
| n      | Subsidiary n |

Examples:

A lone cord with no subsidiaries:

    [99,0]

A cord with four connected subsidiaries with ids 100,101,102,103

    [99,4,
	  [100,0],
	  [101,0],
	  [102,0],
	  [103,0]]

A cord with four chained sub-subsidiaries 

    [99,1,
	  [100,1,
	    [101,1,
		  [102,1,
		    [103,0]]]]]

  
### Frequency based approach

Pros:

 - Potentially higher availibility components.
 - More interesting implementation.
 - Could include audible frequencies.

Cons:

 - Higher complexity.
 - A larger circuit.
 - One more connection needed - power, signal, base frequency and child spread.

Each cord as a voltage controlled oscillator, contributing to a single
returning signal path where the base frequency and spread for it's
children are defined by it's parent.

                      low frequency -------------------------> high frequency
    primary           |                          |                          |
    pendants          |        |        |        |        |        |        |
	subsidiaries      |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
	sub-subsidiaries  |||||||||||||||||||||||||||||||||||||||||||||||||||||||

Cord knot ID would be encoded into the signal via e.g. amplitude or
phase modulation.

Although a purely analogue solution, the hierarchical structure
reconstruction could be done via DSP.

### Microprocessor based build details

#### Plug selection

Options:

  - Off the shelf vs self build
  - Use cases: visually impaired users, in the dark/algorave
  - Avoid needing to get orientation right if possible
  - Larger = better

Audio jacks are good as they don't need orientation (which removes
older USB, MIDI, DIN connectors etc) Seems we can't get 4 ring 1/4
inch jacks - USB C would work though, although perhaps too small.

Custom connectors have been problematic for us in the past (Viruscraft
V1) but PCB edge connectors might be doable - e.g. encasing PCB in
resin would solve a lot of problems (and make it waterproof).

#### Haptic feedback

For use with visually impaired people, but also as a haptic indicator
of dynamic information - e.g. "this expressions is currently being
evaluated" or "this note is currently playing".

Examples - tiny phone vibration motor embedded in PCB, which can be
controlled externally.

## Sketch

Using resin to embed the pcb and components with the plug itself -
easier than fitting it inside the standard plug body and allows an LED
to be visible inside. Unclear if the haptic feedback motor would have
problems being embedded too.
  
![](https://codeberg.org/nebogeo/khipu/raw/branch/master/design/sketch/sketch.png)

## First prototype

![](https://codeberg.org/nebogeo/khipu/raw/branch/master/pics/proto-0001.jpg)

Attiny84 on the breadboard and SMD version on the PCB, running the
same code and talking to each other. "Upward" communications to the
parent using hardware USI on dedicated SDA/SCL pins (as this direction
is harder to implement) and the "downward" connection to child
pendants is using a software ["bit bang" implementation](https://github.com/alx741/avr_i2c/) of the i2c
protocol which can be configured to use any digital pins. This
arrangement is quite unusual to run at the same time but seems to
work.
    
![](https://codeberg.org/nebogeo/khipu/raw/branch/master/pics/schematic-0001.jpg)

### Ordered child pendants

A problem with having a single downward pendant i2c connection, is
that while we can use it to connect arbitrary amounts of sockets and
subpendants, we cannot determine the order - which sockets the plugs
are connected to (as they are just on a shared bus connection).

The software i2c implementation can be hacked to use different pins
for SDA (and share a single downward SCL connection).

With only a single connection per i2c bus there is a question as to
whether this is the right protocol for the job, but it seems similar
approaches using sofware based UART are less supported and flexible
(as they would need to implement both read and write).

New schematic with 4 child pendant connections, a motor driver circuit
and indicator LED:
    
![](https://codeberg.org/nebogeo/khipu/raw/branch/master/pics/schematic-0002.jpg)

### First iteration of PCB routing

![](https://codeberg.org/nebogeo/khipu/raw/branch/master/pics/routing-0002.png)

![](https://codeberg.org/nebogeo/khipu/raw/branch/master/pics/render-0002.png)

Building pendant cord tie point into PCB as above.

### Prototype v1 construction

![](pics/soldering-1.jpg)
![](pics/proto-1.jpg)

Trying out with the breadboard version, but immediately need another for proper testing.

### Prototype v2 construction

New smaller PCB design

![](pics/cnc.jpg)
![](pics/soldering-2.jpg)
![](pics/proto-2.jpg)


Tested together, works well - reset on plug though due to voltage
drop, probably not an issue. Need to check brownout fuse setting on
the Makefile as that might be overzealous for this application.

Haptic feedback works really well, pulsing for ID/location detection.

    
More thoughts:

- quite a limited number of pendants needed to do stuff - eg

Parents
1 led [on/off]
2 walk [type] [steps]
3 wait [time]


- paperwhite debug screen for workshop
- run on teensy/esque (yarnc.scm??)
