# Khipu interface design

A tangible interface that allows construction and manipulation of
hierarchical tree structures, inspired by khipu from the Inca
civilisation.

**Why?** We see the [limitations of our predominant thinking aids
(i.e. computational
technology)](http://worrydream.com/TheHumaneRepresentationOfThought/note.html)
as increasingly holding back our society from good decision making, by
relegating different forms of experience to "secondary status". In
order to counter this it is necessary to look to history for [other
societies who also use digital technologies to store, process and
manipulate information](https://khipumantes.github.io/).

As with our work on the [Penelope
project](https://penelope.hypotheses.org/), we consider the Inca as
holders of precious information and practises that have potential to
alleviate serious and widespread problems in the predominant
sociotechnical situation of today.

This is a prototype that elevates touch and physical manipulation to a
higher status in programming, up to now a field considered to exist in
a mythical plane of "abstract thought" known as "cyberspace". Perhaps
we can use it to explore the ways this strongly restricted,
disembodied thinking is in fact making us stupider, overall.

There are a couple of different ways to approach this problem.

### Microprocessor based approach

Pros:

 - A standard "straightforward" technique.
 - Made from relatively cheap parts.
 - Suitable for mass production.

Cons:

 - Requires lots of microprocessors, and obtaining them is getting harder.
 - Unfortunate terminology regarding serial communication, which we shall refer to as parent and child.

The attiny85 is one of the smallest microprocessors (and one we have
already used in the pattern matrix) with 8 pins. It comes with built
in hardware i2c serial communication, and a secondary serial bus can
be emulated on two of the remaining pins.

Using i2c we can theoretically support 112 addresses.

#### Diagram 1: 4 ring mini jack plug arrangement

![](https://codeberg.org/nebogeo/khipu/raw/branch/master/design/pics/cables.png)

Each pendant microprocessor would be responsible for reading all its
downward subsidiary "child" cords and reporting their ids (and
attached sub ids) to their parent. In the case of a cord attached to
the primary cord it would report it to the attached device via a USB
connection.

Each ID would be unique and would be mapped to the knots on the cord -
so the entire structure could be recreated in software. The downside
is that the knots would not directly be readable this way, but would
still provide a tactile method for organising information.

#### Diagram 2: ATTINY85 pinout

![](https://codeberg.org/nebogeo/khipu/raw/branch/master/design/pics/index.jpeg)

Pins 5 and 7 are used for the hardware i2c (sda and scl), which are
best suited to the upward parent connection, leaving e.g. pins 3 and 4
for a software i2c bus to connect "downward" to the child cords.

#### Scanning process

Each cord's microprocessor would await for a "scan" instruction (via a
register write), when detected it would issue scan instructions to all
detected cords on it's subsidiary bus. After waiting for all
subsidiary cords to indicate completion (via another register) it
would then read the ids reported for each subsidiary and indicate
completion to the parent.

#### Register map

| Address | Description | Notes | R/W |
|---------|-------------|-------|-----|
| 0x00    | Cord ID     | Same as i2c address | R |
| 0x01    | Start scan  |       | W |
| 0x02    | Scan complete |     | R |
| 0x03    | Start of cord data | | R |

#### Cord data

A nested data structure packed into bytes: 

| Offset | Name |
|--------|------|
| 0      | ID   |
| 1      | Size |
| 2      | Subsidiary 1 |
| n      | Subsidiary n |

Examples:

A lone cord with no subsidiaries:

    [99,0]

A cord with four connected subsidiaries with ids 100,101,102,103

    [99,4,
	  [100,0],
	  [101,0],
	  [102,0],
	  [103,0]]

A cord with four chained sub-subsidiaries 

    [99,1,
	  [100,1,
	    [101,1,
		  [102,1,
		    [103,0]]]]]

  
### Frequency based approach

Pros:

 - Potentially higher availibility components.
 - More interesting implementation.
 - Could include audible frequencies.

Cons:

 - Higher complexity.
 - A larger circuit.
 - One more connection needed - power, signal, base frequency and child spread.

Each cord as a voltage controlled oscillator, contributing to a single
returning signal path where the base frequency and spread for it's
children are defined by it's parent.

                      low frequency -------------------------> high frequency
    primary           |                          |                          |
    pendants          |        |        |        |        |        |        |
	subsidiaries      |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
	sub-subsidiaries  |||||||||||||||||||||||||||||||||||||||||||||||||||||||

Cord knot ID would be encoded into the signal via e.g. amplitude or
phase modulation.

Although a purely analogue solution, the hierarchical structure
reconstruction could be done via DSP.

